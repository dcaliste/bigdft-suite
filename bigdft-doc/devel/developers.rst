BigDFT-suite manifesto
======================

The code is not a monolithic piece of software but a collection of *independent*
packages that **may** be installed independently.
The :ref:`Installer <installer-script-label>` script has been
designed for the purpose.

Tips for developers
===================

How to use git
--------------

The repository of bigdft-suite is in gitlab_. For each developer, it is safe to clone its own repository.

.. _gitlab: https://gitlab.com/l_sim/bigdft-suite

Each developer has its own devel branch which has to be a copy of the bigdft-suite branch (called **main**). 
Only the command ``git pull main devel`` will be used to update your **devel** branch.
It is highly recommended not to develop in your **devel** branch!

For each set of modifications, start a new branch with ``git checkout -B newbranch`` from the **devel** one.
Then you do your modifications, commits and so on.

Before the merge request, you have to update your **newbranch** branch from the **devel** branch 
(which will be updated from the main one) with ``git rebase devel`` and solve the conflicts if necessary.

Then go to gitlab_ and do a merge-request. Finally do not change your **newbranch** branch because the merge request will be
affected. In function of the remarks of the reviewer, you change you **newbranch** branch, do a ``git pull origin newbranch`` to propagate the commits. 

Choose the correct package in which to insert the developments
--------------------------------------------------------------

The first question to ask yourself is the *generality* of the
functionality you are going to implement.
The spirit is to work at the lowest possible level for a given task.
The idea is to make available the functionality also to other potential
users of the ``BigDFT-suite`` subpackages.
This will also help in a better structure of the API of each package.

For instance, suppose you would like to implement a continuum solvent
cavity determination for a particular DFT run of a molecular system.
The correct level of development in this case would be the ``psolver``
package, as this is presently dealing with continuum solvents and cavities.

For a general overview one might say that:

 * ``futile`` deals with low-level functionalities like ``stdlib`` (but for FORTRAN).
   New MPI wrappers, strategies for memory copy and allocations should be implemented there.

 * ``at_lab`` library (will) deal with all the operations which are associated to position

 * <to_B_continued>


Read the coding rules
---------------------

For some inspiration on coding style and strategies, please read:

.. toctree::
   :maxdepth: 2

   coding_rules


Create a test for the functionality
-----------------------------------

Each of the packages has its own continuous integration procedure,  refer to
it for a suitable implementation.

 * ``futile``, ``psolver``: F_REGTEST_INSTRUCTION (to be documented)
 * ``bigdft`` see here_.

.. _here: http://bigdft.org/Wiki/index.php?title=Inserting_a_new_test_in_the_distribution


Build and improve the BigDFT-suite's documentation
--------------------------------------------------

From version 1.8.3, sphinx is used to build the documentation.

The file ``requirements.txt`` gives the list of python packages used to build the documentation.
which can be installed with

``pip install -r requirements.txt``

Then, perform in the source directory

``make html``


Sphinx uses the `restructed format <http://www.sphinx-doc.org/en/master/usage/restructuredtext>`_ and can also extract
from `python docstrings <https://devguide.python.org/documenting>`_ the documentation for the python modules.


The full documentation can be installed in `website <https://readthedocs.org/projects/bigdft-suite>`_.

Document the API of the high-level routines
-------------------------------------------

.. todo:: write something here

PyBigDFT Development Concepts
-----------------------------

.. toctree::
   :maxdepth: 2

   pybigdft_devel

