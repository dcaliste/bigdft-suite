!> @file
!!   Basic routines for using bigpoly.
!! @author
!!   Copyright (C) 2016 CheSS developers
!!
!!   This file is part of CheSS.
!!   
!!   CheSS is free software: you can redistribute it and/or modify
!!   it under the terms of the GNU Lesser General Public License as published by
!!   the Free Software Foundation, either version 3 of the License, or
!!   (at your option) any later version.
!!   
!!   CheSS is distributed in the hope that it will be useful,
!!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!   GNU Lesser General Public License for more details.
!!   
!!   You should have received a copy of the GNU Lesser General Public License
!!   along with CheSS.  If not, see <http://www.gnu.org/licenses/>.


!> Basic routines for using bigpoly.
module bigpoly
  use futile
  use processgridmodule, only : constructprocessgrid, destructprocessgrid
  use sparsematrix_timing
  implicit none
  private

  type(dictionary), pointer, public :: bigpoly_options
  logical, parameter, public :: has_ntpoly = .true.

  !> Public routines
  public :: set_bigpoly_parameters

  contains

    !> Set the parameters for bigpoly
    subroutine set_bigpoly_parameters(options)
      !> The dictionary specifying the parameters of the calculation.
      type(dictionary), intent(in), pointer :: options

      call f_routine(id='set_bigpoly_parameters')

      bigpoly_options => options

      call f_release_routine()
    end subroutine set_bigpoly_parameters

end module bigpoly
